# coala-jencapsulation
Simple java static analysis tool for checking, if a java class is correctly 
encapsulated. That means checking that:
1. class contains one public class
2. class attributes are not correctly declared (not private)
3. class contains getters and setters of declared attributes

This tool is implemented by Coala, as a static analysis tool called Bear.
(convention of the project)
This code is only a module which you can attach to coala, so it won't be 
working standalone.

JEncapsulationBear searches through a specified file and creates errors 
with patches, which you can apply interactively or non-interactively
by giving an option --apply-patches to the console.

For more information about the usage and documentation, please visit the
official coala project site: https://coala.io/#/home?lang=Python