package cz.muni.fi.pb162.hw01.impl;

import cz.muni.fi.pb162.hw01.DefaultPortResolver;
import cz.muni.fi.pb162.hw01.url.SmartUrl;

import java.util.Arrays;
import java.util.Stack;

/**
 * @author Marek Soska
 */
private class Url implements SmartUrl {
    private String urlString;

    /**
     * Initialises object with url-string.
     *
     * @param url Given url-string.
     */
    public Url(String url) {
        this.urlString = url.trim();
    }

    private String getPortFromString() {
        try {
            return urlString.split("/")[2].split(":")[1];
        } catch (Exception e) {
            return null;
        }
    }

    private String getOriginalPath() {
        try {
            return urlString.split("/", 4)[3].split("\\?")[0];
        } catch (Exception e) {
            return null;
        }
    }

    private int getDefaultPort() {
        DefaultPortResolver resolver = new DefaultPortResolver();
        return resolver.getPort(getProtocol());
    }

    private boolean checkLevelDomain(String host) {
        String[] parts = host.split(".");

        if (host.charAt(host.length() - 1) == '.') {
            return false;
        }
        for (String part : parts) {
            if (part.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getAsString() {
        if (getProtocol() == null || getHost() == null || !checkLevelDomain(getHost())) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getProtocol());
        builder.append("://");
        builder.append(getHost());
        if (getPort() != getDefaultPort()) {
            builder.append(":");
            builder.append(getPort());
        }
        if (!getPath().isEmpty()) {
            builder.append("/");
            builder.append(getPath());
        }
        if (!getQuery().isEmpty()) {
            builder.append("?");
            builder.append(getQuery());
        }
        if (!getFragment().isEmpty()) {
            builder.append("#");
            builder.append(getFragment());
        }
        return builder.toString();
    }

    @Override
    public String getAsRawString() {
        return urlString;
    }

    @Override
    public boolean isSameAs(SmartUrl url) {
        return this.getAsString().equals(url.getAsString());
    }

    @Override
    public boolean isSameAs(String url) {
        return isSameAs(new Url(url));
    }

    @Override
    public String getHost() {
        try {
            return urlString.split("/")[2].split(":")[0];
        } catch (Exception noHost) {
            return null;
        }
    }

    @Override
    public String getProtocol() {
        if (!urlString.contains("://")) {
            return null;
        }
        try {
            return urlString.split("://")[0];
        } catch (Exception e){
            return null;
        }
    }


    @Override
    public int getPort() {
        String portStr = getPortFromString();
        if (portStr != null) {
            return Integer.parseInt(portStr);
        }
        return getDefaultPort();
    }

    @Override
    public String getPath() {
        if (getOriginalPath() == null) {
            return "";
        }
        String[] items = getOriginalPath().split("/");
        Stack<String> stack = new Stack<>();

        for (String item : items) {
            if (item.equals(".")) {
                continue;
            }
            if (item.equals("..")) {
                try {
                    stack.pop();
                    continue;
                } catch (Exception outsideOfRoot) {
                    return "";
                }
            }
            stack.push(item);
        }

        StringBuilder builder = new StringBuilder();
        for (String item : stack) {
            builder.append(item);
            builder.append('/');
        }
        if (!stack.isEmpty()) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    private String getSortedQuery(String query) {
        StringBuilder builder = new StringBuilder();

        String[] parameters = query.split("&");
        Arrays.sort(parameters);

        for (String item : parameters) {
            builder.append(item);
            builder.append('&');
        }
        if (parameters.length != 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    @Override
    public String getQuery() {
        String query = "";
        try {
            query = urlString.split("\\?")[1].split("#")[0];
        } catch (Exception e) {
            return query;
        }
        return getSortedQuery(query);
    }

    @Override
    public String getFragment() {
        try {
            return urlString.split("#")[1];
        } catch (Exception e) {
            return "";
        }
    }

    public static class SomeClass {
        private String someAttribute;

        public SomeClass(String someAttribute) {
            this.someAttribute = someAttribute;
        }
    }
}
