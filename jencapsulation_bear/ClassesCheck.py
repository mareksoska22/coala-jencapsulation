import re 

class ClassesCheck:
    def __init__(self, filename, file):
        self.filename = filename
        self.lines = file

        self.read_ptr = -1

        self.class_body_dict = {}
        self.class_error_dict = {}

        self.class_lastline_dict = {}

    
    def run_check(self):
        self.__extract_class_body(None)
        for class_name in self.class_body_dict.keys():
            if not self.class_lastline_dict.get(class_name):
                self.class_lastline_dict[class_name] = len(self.lines)
            

        
    def __extract_class_body(self, actual_class):
        self.read_ptr += 1

        if actual_class:
            self.class_body_dict[actual_class] = []
            body_stack = ['{']

        while self.read_ptr < len(self.lines):
            line = self.lines[self.read_ptr]
            self.__validate_indentation_signs(line)

            if actual_class:
                if '{' in line:
                    body_stack.append('{')
                if '}' in line:
                    body_stack.pop()
                if not body_stack:
                    self.class_lastline_dict[actual_class] = self.read_ptr
                    return

            class_definition = self.__parse_class_definition(line)
            if class_definition:
                prefix, name = class_definition

                validation_result = self.__validate_class_definition(actual_class, prefix, name)
                if validation_result:
                    self.class_error_dict[name] = validation_result

                self.__extract_class_body(name)
                self.read_ptr += 1
            else:
                if actual_class:
                    self.class_body_dict[actual_class].append(line)
                self.read_ptr += 1



    def __parse_class_definition(self, line):
        visibility_class = re.search(r"\s*(.+?)\s+class\s", line)
        no_visibility_class = re.search(r"^\s*class\s", line)

        if visibility_class or no_visibility_class:
            if '{' not in line:
                exit("The class definition should contain a brace on line: "
                    + str(self.read_ptr + 1) + ", exiting bear.")
            try:
                if visibility_class:
                    prefix = visibility_class.group(1)
                else:
                    prefix = ''
                name = re.search(r"class\s+(.+?)\s", line).group(1)
            except SyntaxError as e:
                raise e
            return prefix, name


    def __validate_class_definition(self, actual_class, prefix, name):
        if self.class_body_dict.get(name):
            return "Multiple class " + name + " definition."

        if name == self.filename.split(".")[0]:
            if actual_class != None:
                return "The class " + name + " should not be nested."
            if "public" not in prefix:
                return "The class " + name + " should be public."
            return

        if actual_class == None:
            if "public" in prefix:
                return "The class " + name + " shouldn't be public."
            if "private" in prefix:
                return "The class " + name + " shouldn't be private."


    def __validate_indentation_signs(self, line):
        if re.search(r"{.*{", line) or re.search(r"}.*}", line):
            exit("Bad braces formatting on line: " 
                + str(self.read_ptr + 1) + ", exiting bear.")

        if re.search(r";.*;", line):
            exit("Bad semicolon formatting on line: "
                + str(self.read_ptr + 1) + ", exiting bear.")
