import re

from GetterSetter import GetterSetter

class AttributesCheck:
    def __init__(self, class_name, class_body):
        self.class_name = class_name
        self.class_body = class_body

        self.no_methods_body = []
        self.methods_body_dict = {}

        self.read_ptr = 0

        self.attributes = []
        self.attributes_error_dict = {}
        self.attributes_patch_dict = {}
       

    def run_check(self):
        self.__extract_methods_body()
        self.__extract_attributes()


    def __get_line(self):
        if self.read_ptr < len(self.class_body):
            return self.class_body[self.read_ptr]


    def __extract_methods_body(self):
        while self.__get_line():
            method_definition = self.__parse_method_definition(self.__get_line())

            if method_definition:
                prefix, return_type, name = method_definition

                self.methods_body_dict[name] = []
                body_stack = ['{']

                while self.__get_line():
                    self.read_ptr += 1
                    if '{' in self.__get_line():
                        body_stack.append('{')
                    if '}' in self.__get_line():
                        body_stack.pop()
                    if not body_stack:
                        self.read_ptr += 1
                        break
                    self.methods_body_dict[name].append(self.__get_line())
            else:
                self.no_methods_body.append(self.__get_line())
                self.read_ptr += 1


    def __parse_method_definition(self, line):
        method_definition = re.search(r"^(.*?)(\w+?)\s+(\w+?)\s*\(.*\)", line)

        if method_definition:
            name = method_definition.group(3)
            return_type = method_definition.group(2)
            prefix = method_definition.group(1).strip()

            if '{' not in line:
                exit("The method " + name + " definition should contain a brace on the same line"
                    + ", exiting bear.")
        
            if name == self.class_name:
                return (prefix + return_type, '', name)
            return prefix, return_type, name



    def __extract_attributes(self):
        for line in self.no_methods_body:
            attribute_definition = re.search(r"^(.*?)(\w+?)\s+(\w+?);", line)
            if attribute_definition:
                name = attribute_definition.group(3)
                return_type = attribute_definition.group(2)
                prefix = attribute_definition.group(1).strip()
                self.__validate_attribute_definition(prefix, return_type, name)
                

    def __validate_attribute_definition(self, prefix, return_type, name):
        if name in self.attributes:
            self.attributes_error_dict[name] = "Multiple attribute " + name + " definition."
            return
        self.attributes.append(name)

        if "static" in prefix and "final" in prefix:
            return

        if "private" not in prefix:
            self.attributes_error_dict[name] = "The attribute " + name + " should be private."
            return
        
        self.attributes_patch_dict[name] = []
        gen = GetterSetter(return_type, name)

        if not self.methods_body_dict.get(gen.getterPattern()):
            self.attributes_patch_dict[name].append(gen.generateGetter())
            
        if not self.methods_body_dict.get(gen.setterPattern()):
            self.attributes_patch_dict[name].append(gen.generateSetter())
    



    