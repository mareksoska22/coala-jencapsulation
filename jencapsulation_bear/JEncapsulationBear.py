import logging
import ntpath

from ClassesCheck import ClassesCheck
from AttributesCheck import AttributesCheck

from coalib.bears.LocalBear import LocalBear
from bears.general.IndentationBear import IndentationBear

from coalib.results.Result import Result
from coalib.results.Diff import Diff


class JEncapsulationBear(LocalBear):
    LANGUAGES = {'Java'}
    AUTHORS = {'Marek Soska'}
    AUTHORS_EMAILS = {'mareksoska22@gmail.com'}
    CAN_DETECT = {'Syntax'}
    CAN_FIX = {'Security', 'Variable Misuse'}
    
    BEAR_DEPS = {IndentationBear}

    def run(self, filename, file, dependency_results):
        single_filename = ntpath.basename(filename)

        logging.debug("Running classes check.")
        classes_check = ClassesCheck(single_filename, file)
        classes_check.run_check()

        for errored_class in classes_check.class_error_dict.keys():
            message = classes_check.class_error_dict[errored_class]

            yield Result.from_values(
                origin=self,
                message=message,
                file=filename
            )
        
        logging.debug("Running attributes check.")
        for class_name in classes_check.class_body_dict.keys():
            if not classes_check.class_lastline_dict.get(class_name):
                yield self.new_result(message="The class " + class_name + " is probably not correctly ended, " +
                    "skipping getters setters generating.", file=filename)
                continue

            attributes_check = AttributesCheck(
                class_name, 
                classes_check.class_body_dict[class_name]
            )
            attributes_check.run_check()

            for errored_attribute in attributes_check.attributes_error_dict.keys():
                message = attributes_check.attributes_error_dict[errored_attribute]

                yield Result.from_values(
                    origin=self,
                    message=message,
                    file=filename
                )
            attributes_check.run_check()
            
            
            for patched_attribute in attributes_check.attributes_patch_dict.keys():
                for patch in attributes_check.attributes_patch_dict[patched_attribute]:
                    diff = Diff(file)
                    diff.add_lines(
                        line_nr_before=classes_check.class_lastline_dict[class_name],
                        lines=patch.split('\n'))

                    yield Result.from_values( 
                        origin=self, 
                        message="The attribute " + patched_attribute + " is missing this method.",
                        file=filename,
                        diffs={filename : diff}
                    )
                
        
       
        
    
    