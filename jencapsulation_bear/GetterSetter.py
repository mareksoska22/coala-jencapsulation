import re 

class GetterSetter:
    def __init__(self, return_type, name):
        self.return_type = return_type
        self.name = name


    def getterPattern(self):
        return "get" + self.name[0].capitalize() + self.name[1:]

    
    def setterPattern(self):
        return "set" + self.name[0].capitalize() + self.name[1:]


    def generateGetter(self):
        return "\npublic " + self.return_type + " " + self.getterPattern() + "() {\n    return " + self.name + ";\n}" 


    def generateSetter(self):
        return "\npublic void " + self.setterPattern() + "(" + self.name + ") {\n    this." + self.name + " = " + self.name + ";\n}" 

